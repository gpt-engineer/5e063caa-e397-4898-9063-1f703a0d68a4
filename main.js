// Function to delete a task
function deleteTask(e) {
    if (e.target.classList.contains('delete-task')) {
        e.target.parentElement.remove();
    }
}

// Function to edit a task
function editTask(e) {
    if (e.target.classList.contains('edit-task')) {
        const task = e.target.parentElement;
        const taskText = task.textContent;
        task.innerHTML = `<input type="text" class="task-input" value="${taskText}">`;
        task.querySelector('.task-input').focus();
    }
}

// Function to update a task
function updateTask(e) {
    if (e.target.classList.contains('task-input')) {
        const task = e.target.parentElement;
        const taskText = e.target.value;
        task.innerHTML = `<div class="task-text">${taskText}</div><div><button class="edit-task">Edit</button></div>`;
    }
}

// Function to add a task
function addTask() {
    const taskList = document.getElementById('to-do-list');
    const newTask = document.createElement('div');
    newTask.className = 'bg-gray-300 rounded p-2 mb-2 shadow-md drop-in';
    newTask.innerHTML = 'New Task <button class="edit-task">Edit</button>';
    taskList.appendChild(newTask);
}

// Event listeners
document.getElementById('add-task').addEventListener('click', addTask);
document.addEventListener('click', deleteTask);
document.addEventListener('click', editTask);
document.addEventListener('blur', updateTask, true);